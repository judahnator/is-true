<?php

use judahnator\IsTrue\IsTrue;
use PHPUnit\Framework\TestCase;

final class IsTrueTest extends TestCase
{

    public function testIsTrueClass(): void
    {
        $this->assertTrue((new IsTrue(true))->evaluate(), 'True evaluated to False!');
    }

    public function testIsTrueFunction(): void
    {
        $this->assertTrue(is_true(true), 'True evaluated to False!');
    }

}