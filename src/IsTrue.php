<?php

namespace judahnator\IsTrue;

final class IsTrue
{

    private $item;

    public function __construct(bool $item)
    {
        $this->item = $item;
    }

    public function evaluate(): bool
    {
        return
            $this->item === true &&
            $this->item !== false &&
            !(!$this->item);
    }

}