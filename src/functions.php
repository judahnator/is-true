<?php

use judahnator\IsTrue\IsTrue;

function is_true(bool $item): bool
{
    return (new IsTrue($item))->evaluate();
}